package es.cipfpbatoi;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Activitat 7. Crea un mètode getEdat() que demane a l'usuari un número entre 10 i 50.
 * Si el número no es troba al rang,
 * haurà de llançar una excepció de tipus InputMismatchException amb un missatge
 * descriptiu de l'error que s'ha produït.
 * Captura des del main() l'excepció anterior i mostra el missatge.
 *
 * Prova el mètode amb els valors 10, 20, 50, 200 i 9.
 */
public class Activitat7 {


    public static final int MIN_EDAT = 10;
    public static final int MAX_EDAT = 50;

    public static void main(String[] args) {
        try {
            int edat  = getEdat();
            System.out.println("El numero introducido es " + edat);
        } catch (InputMismatchException e) {
            System.out.println(e.getMessage());
        }
    }

    public static int getEdat() {
        Scanner teclat = new Scanner(System.in);
        System.out.println("Introdueix la edat");
        int edat = teclat.nextInt();
        if (edat < MIN_EDAT || edat > MAX_EDAT) {
            throw new InputMismatchException(
                    String.format("El número debe estar entre %d y %d", MIN_EDAT, MAX_EDAT));
        }
        return edat;
    }
}
