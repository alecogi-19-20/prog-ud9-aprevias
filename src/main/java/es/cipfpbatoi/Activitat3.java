package es.cipfpbatoi;

import java.util.InputMismatchException;
import java.util.Scanner;
/**
 * Actividad3.- Escribe un método que se encargue de pedir números al usuario hasta que se indique un número entre 1 y 5
 * (ambos incluidos). Cada número que incluya debe concatenarse en una variable de tipo String.
 *  Al finalizar la ejecución debe visualizar un mensaje similar a: "Números introducidos: 10, 20,12, 56, 30".
 * Nota: Se debe validar que el usuario no inserte letras.
 */
public class Activitat3 {

    public static void main(String[] args) {

        System.out.println("Num Introducidos: " + getNumbers());
    }

    public static String getNumbers() {
        Scanner scanner = new Scanner(System.in);
        StringBuilder stringBuilder = new StringBuilder();
        do {
            System.out.println("Introduce un número");
            try {
                int number = scanner.nextInt();
                if (number >= 1 && number <= 5 ){
                    return stringBuilder.toString();
                }
                if (stringBuilder.length() > 0) {
                    stringBuilder.append(",");
                }
                stringBuilder.append(number);
            } catch (InputMismatchException e) {
                System.out.println("Debe introducir un entero");
                scanner.nextLine();
            }
        } while (true);
    }
}
