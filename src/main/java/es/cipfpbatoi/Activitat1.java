package es.cipfpbatoi;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Activitat 1. Realitza un programa que demani 6 números per teclat
 * i ens digui quin és el màxim. Si l'usuari introdueix una dada errònia
 * (que no sigui un nombre sencer)
 * el programa ha d'indicar-ho i ha de tornar a demanar el número.
 */
public class Activitat1
{

    public static final int TOTAL_NUMBERS = 6;

    public static void main(String[] args )
    {

        Scanner teclat = new Scanner(System.in);
        int maxim = Integer.MIN_VALUE;
        for (int i = 0; i < TOTAL_NUMBERS; i++) {
            try {
                int numero = teclat.nextInt();
                if (numero > maxim) {
                    maxim = numero;
                }
            } catch (InputMismatchException e) {
                System.out.println("Debe introducir un entero");
                i--;
                teclat.nextLine();
            }
        }
        System.out.printf("El maximo introducido es %d", maxim);

    }
}
