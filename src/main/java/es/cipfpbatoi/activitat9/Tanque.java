package es.cipfpbatoi.activitat9;

import es.cipfpbatoi.activitat9.exceptions.TancBuitException;
import es.cipfpbatoi.activitat9.exceptions.TancPleException;

public class Tanque {

    private float capacitat;

    private float carregaActual;

    public Tanque(float capacitat) {
        this.capacitat = capacitat;
        this.carregaActual = 0;
    }

    public float getCapacitatRestant() {
        return capacitat - carregaActual;
    }
    public void afegir(float litres) {
        if ((litres + carregaActual) > capacitat) {
            throw new TancPleException();
        }
        carregaActual += litres;
    }

    public void retirar(float quantitat) {
        if (quantitat > carregaActual) {
            throw new TancBuitException();
        }
        carregaActual -= quantitat;
    }
}
