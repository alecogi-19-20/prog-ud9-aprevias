package es.cipfpbatoi.activitat9;

import es.cipfpbatoi.activitat9.exceptions.TancBuitException;
import es.cipfpbatoi.activitat9.exceptions.TancPleException;

public class TesTTanque {

    public static void main(String[] args) {

        Tanque tanqueAgua = new Tanque(100);
        tanqueAgua.afegir(50);
        tanqueAgua.retirar(50);
        Tanque tanqueAceite = new Tanque(80);
        if (tanqueAceite.getCapacitatRestant() > 100) {
            tanqueAceite.afegir(100);
        }

    }
}
