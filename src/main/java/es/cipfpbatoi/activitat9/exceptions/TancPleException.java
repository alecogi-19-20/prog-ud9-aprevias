package es.cipfpbatoi.activitat9.exceptions;

public class TancPleException extends RuntimeException {

    public TancPleException() {
        super("El tanc no té suficient volumen disponible");
    }
}
