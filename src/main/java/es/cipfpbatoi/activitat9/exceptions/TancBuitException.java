package es.cipfpbatoi.activitat9.exceptions;

public class TancBuitException extends RuntimeException {

    public TancBuitException(){
        super("No hi ha prou cantitat de liquit");
    }
}
