package es.cipfpbatoi;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Activitat 2. Modifica l’activitat 1 de manera que es dispose d'un mètode obtenirEnter()que demaneu a l'usuari que introdueixi un nombre enter.
 * Aquest mètode serà cridat des del mètode principal main() tantes vegades com calgui perquè l'usuari introdueixi
 * els 6 números enters. Al final s’haurà de mostrar el més gran.
 */
public class Activitat2
{

    private final static Scanner teclat = new Scanner(System.in);
    public static final int TOTAL_NUMBERS = 6;

    public static void main(String[] args )
    {
        int maxim = Integer.MIN_VALUE;
        for (int i = 0; i < TOTAL_NUMBERS ; i++) {
            try {
                int numero = obtenirEnter();
                if (numero > maxim) {
                    maxim = numero;
                }
            } catch (InputMismatchException e) {
                System.out.println("Has d'introduïr un enter");
                teclat.nextLine();
                i--;
            }
        }
        System.out.println("El màxim es " + maxim);
    }

    private static int obtenirEnter() {
        System.out.print("Introdueix un enter: ");
        return teclat.nextInt();
    }
}
