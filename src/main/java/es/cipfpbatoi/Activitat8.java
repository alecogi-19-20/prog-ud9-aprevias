package es.cipfpbatoi;

public class Activitat8 {

    public static void main(String[] args) {
        try {
            waitSeconds(2);
        } catch (InterruptedException e) {
            System.out.println("No se ha podido para la ejecución del programa");
        }
    }
    public static void waitSeconds(int numSeconds) throws InterruptedException {
        Thread.sleep((numSeconds * 1000));
    }
}
