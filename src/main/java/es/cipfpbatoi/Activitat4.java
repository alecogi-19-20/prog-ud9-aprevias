package es.cipfpbatoi;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Actividad4.- Escribe un programa que lea por teclado una secuencia de números hasta que el usuario inserte letras.
 * Al finalizar el programa, se debe visualizar el número más alto de todos los que se hayan introducido.
 * Puede ocurrir que un usuario no llegue a insertar ningún número. En este caso, se indicará con el mensaje correspondiente.
 * Nota: Se debe emplear un método para pedir los números
 */
public class Activitat4 {

    private final static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        Integer major = getMajorNumber();
        if (major == null) {
            System.out.println("No se ha introducido ningún número");
        } else {
            System.out.println("El mayor de los números introducidos es " + major);
        }
    }

    public static Integer getMajorNumber(){
        Integer major = null;
        do {
            try {
                System.out.println("Introduce un número");
                int current = input.nextInt();
                if (major == null || current > major) {
                    major = current;
                }
            }catch (InputMismatchException e) {
                System.out.println("Debía haber introducido números enteros :-) ...");
                return major;
            }
        } while (true);
    }

}

