package es.cipfpbatoi.activitat9b;

public class Tanque {

    private float capacitat;

    private float carregaActual;

    public Tanque(float capacitat) {
        this.capacitat = capacitat;
        this.carregaActual = 0;
    }

    public void afegir(float litres) throws TancPleException {
        if ((litres + carregaActual) > capacitat) {
            throw new TancPleException();
        }
        carregaActual += litres;
    }

    public void retirar(float quantitat) throws TancBuitException {
        if (quantitat > carregaActual) {
            throw new TancBuitException();
        }
        carregaActual -= quantitat;
    }
}
