package es.cipfpbatoi.activitat9b;

public class TesTTanque {

    public static void main(String[] args) {
        try {
            Tanque tanqueAgua = new Tanque(100);
            tanqueAgua.afegir(50);
            tanqueAgua.retirar(50);
            Tanque tanqueAceite = new Tanque(80);
            tanqueAceite.afegir(100);
        } catch (TancBuitException | TancPleException e) {
            System.out.println(e.getMessage());
        }

    }
}
