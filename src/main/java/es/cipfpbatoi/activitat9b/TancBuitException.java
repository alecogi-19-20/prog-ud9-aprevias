package es.cipfpbatoi.activitat9b;

public class TancBuitException extends Exception {

    public TancBuitException(){
        super("No hi ha prou cantitat de liquit");
    }
}
