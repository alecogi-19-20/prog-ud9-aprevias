package es.cipfpbatoi.activitat9b;

public class TancPleException extends Exception {

    public TancPleException() {
        super("El tanc no té suficient volumen disponible");
    }
}
