package es.cipfpbatoi.activitat11.excepcion;

public class SaldoAgotadoExcepcion extends Exception {

    public SaldoAgotadoExcepcion(){
        super("No dispone de saldo para subir al autobus");
    }

}
